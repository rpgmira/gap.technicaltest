# Technical Test for Growth Acceleration Partners

Technical Test for Growth Acceleration Partners in ASP.NET Core by Juan Felipe Mira Uribe

## Tools

* Visual Studio 2019
* .NET Core 2.2
* EntityFramework Core
* SQLite / MySQL
* Bootstrap / Ajax / jQuery

## How To Run

* Open solution in Visual Studio 2019
* Restore NuGet packages
* Set GAP.TechnicalTest project as Startup Project
* Build the project.
* Run the application.