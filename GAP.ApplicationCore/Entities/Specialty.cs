﻿using System.Collections.Generic;

namespace GAP.ApplicationCore.Entities
{
    public class Specialty : EntityBase
    {
        public string Name { get; set; }

        public ICollection<Appointment> Appointments { get; set; }
    }
}