﻿using System.Collections.Generic;

namespace GAP.ApplicationCore.Entities
{
    public class Patient : EntityBase
    {
        public string Name { get; set; }
        public string IdentificationNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public ICollection<Appointment> Appointments { get; set; }

    }
}