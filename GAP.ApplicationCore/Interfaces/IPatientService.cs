﻿using GAP.ApplicationCore.Entities;
using System.Collections.Generic;

namespace GAP.ApplicationCore.Interfaces
{
    public interface IPatientService
    {
        IEnumerable<Patient> GetAll();
        Patient GetById(int patientId);
        bool Create(Patient patient);
        bool Remove(int patientId);
    }
}