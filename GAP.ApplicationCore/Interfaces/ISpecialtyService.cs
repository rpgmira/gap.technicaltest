﻿using GAP.ApplicationCore.Entities;
using System.Collections.Generic;

namespace GAP.ApplicationCore.Interfaces
{
    public interface ISpecialtyService
    {
        IEnumerable<Specialty> GetAll();
        Specialty GetById(int specialtyId);
    }
}