﻿using GAP.ApplicationCore.Entities;
using System.Collections.Generic;

namespace GAP.ApplicationCore.Interfaces
{
    public interface IAppointmentService
    {
        IEnumerable<Appointment> GetAll();

        Appointment GetById(int appointmentId);

        bool Create(Appointment appointment);

        bool Remove(int appointmentId);
    }
}