﻿using GAP.ApplicationCore.Entities;
using System.Collections.Generic;

namespace GAP.ApplicationCore.Interfaces
{
    public interface IRepository<T> where T : EntityBase
    {
        T GetById(int id);
        IEnumerable<T> List();
        bool Add(T entity);
        bool Delete(T entity);
        bool Edit(T entity);
    }
}