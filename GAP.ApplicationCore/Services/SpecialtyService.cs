﻿using System.Collections.Generic;
using GAP.ApplicationCore.Entities;
using GAP.ApplicationCore.Interfaces;

namespace GAP.ApplicationCore.Services
{
    public class SpecialtyService : ISpecialtyService
    {
        private readonly IRepository<Specialty> _specialtyRepository;

        public SpecialtyService(IRepository<Specialty> specialtyRepository)
        {
            _specialtyRepository = specialtyRepository;
        }

        public IEnumerable<Specialty> GetAll()
        {
            var result = _specialtyRepository.List();

            return result;
        }

        public Specialty GetById(int specialtyId)
        {
            var result = _specialtyRepository.GetById(specialtyId);

            return result;
        }
    }
}