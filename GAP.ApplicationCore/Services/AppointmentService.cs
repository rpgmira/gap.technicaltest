﻿using GAP.ApplicationCore.Entities;
using GAP.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GAP.ApplicationCore.Services
{
    public class AppointmentService : IAppointmentService
    {
        private readonly IRepository<Appointment> _appointmentRepository;
        private readonly IPatientService _patientService;
        private readonly ISpecialtyService _specialtyService;

        public AppointmentService(
            IRepository<Appointment> appointmentRepository,
            IPatientService patientService,
            ISpecialtyService specialtyService)
        {
            _appointmentRepository = appointmentRepository;
            _patientService = patientService;
            _specialtyService = specialtyService;
        }

        public IEnumerable<Appointment> GetAll()
        {
            var result = _appointmentRepository.List();

            if (result != null)
            {
                var appointments = new List<Appointment>();

                foreach (var item in result)
                {
                    appointments.Add(ComplementInfo(item));
                }

                return appointments;
            }

            return null;
        }

        public Appointment GetById(int appointmentId)
        {
            var result = _appointmentRepository.GetById(appointmentId);

            if (result != null)
                return ComplementInfo(result);

            return null;
        }

        public bool Create(Appointment appointment)
        {
            try
            {
                var result = CanBeCreated(appointment) ? _appointmentRepository.Add(appointment) : false;

                return result;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Remove(int appointmentId)
        {
            try
            {
                var appointment = _appointmentRepository.GetById(appointmentId);

                var result = CanBeCanceled(appointment) ? _appointmentRepository.Delete(appointment) : false;

                return result;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private bool CanBeCanceled(Appointment appointment)
        {
            // Appointment must be canceled with at least 24 hours in advance
            var canBeCanceled = (appointment.Date - DateTime.Now).TotalHours > 24;

            return canBeCanceled;
        }

        private bool CanBeCreated(Appointment appointment)
        {
            // Appointment can be created if patient doesn't have appointments for the same day
            var patient = _patientService.GetById(appointment.PatientId);
            var patientAppointments = GetAll().Where(x => x.PatientId == patient.Id).ToList();

            var anotherAppointmentForSameDay =
                patientAppointments?.FirstOrDefault(x => x.Date.Date == appointment.Date.Date);

            var canBeCreated = anotherAppointmentForSameDay == null;

            return canBeCreated;
        }

        private Appointment ComplementInfo(Appointment appointment)
        {
            var fullAppointment = new Appointment()
            {
                Id = appointment.Id,
                Date = appointment.Date,
                PatientId = appointment.PatientId,
                SpecialtyId = appointment.SpecialtyId,
                Patient = _patientService.GetById(appointment.PatientId),
                Specialty = _specialtyService.GetById(appointment.SpecialtyId)
            };

            return fullAppointment;
        }
    }
}