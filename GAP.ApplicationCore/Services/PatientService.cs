﻿using GAP.ApplicationCore.Entities;
using GAP.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;

namespace GAP.ApplicationCore.Services
{
    public class PatientService : IPatientService
    {
        private readonly IRepository<Patient> _patientRepository;

        public PatientService(IRepository<Patient> patientRepository)
        {
            _patientRepository = patientRepository;
        }

        public IEnumerable<Patient> GetAll()
        {
            var result = _patientRepository.List();

            return result;
        }

        public Patient GetById(int patientId)
        {
            var result = _patientRepository.GetById(patientId);

            return result;
        }

        public bool Create(Patient patient)
        {
            try
            {
                var result = _patientRepository.Add(patient);

                return result;
            }
            catch (Exception e)
            {
                return false;
            }            
        }

        public bool Remove(int patientId)
        {
            try
            {
                var patient = _patientRepository.GetById(patientId);

                var result = _patientRepository.Delete(patient);

                return result;
            }
            catch (Exception e)
            {
                return false;
            }            
        }
    }
}