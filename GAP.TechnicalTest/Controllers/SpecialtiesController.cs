﻿using GAP.ApplicationCore.Entities;
using GAP.ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace GAP.TechnicalTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpecialtiesController : ControllerBase
    {
        ISpecialtyService _specialtyService;

        public SpecialtiesController(ISpecialtyService specialtyService)
        {
            _specialtyService = specialtyService;
        }

        // GET: api/Specialties
        [HttpGet]
        public IEnumerable<Specialty> Get()
        {
            var result = _specialtyService.GetAll();

            return result;
        }
    }
}