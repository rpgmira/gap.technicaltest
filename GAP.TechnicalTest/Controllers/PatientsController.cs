﻿using GAP.ApplicationCore.Entities;
using GAP.ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace GAP.TechnicalTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientsController : ControllerBase
    {
        IPatientService _patientService;

        public PatientsController(IPatientService patientService)
        {
            _patientService = patientService;
        }

        // GET: api/Patients
        [HttpGet]
        public IEnumerable<Patient> Get()
        {
            var result = _patientService.GetAll();

            return result;
        }
        
        // POST: api/Patients
        [HttpPost]
        public bool Post([FromBody] Patient value)
        {
            var result = _patientService.Create(value);

            return result;
        }
    }
}