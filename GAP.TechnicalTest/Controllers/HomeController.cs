﻿using GAP.TechnicalTest.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace GAP.TechnicalTest.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Patients()
        {
            return View();
        }

        public IActionResult Appointments()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Technical Test for Growth Acceleration Partners";
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Juan Felipe Mira Uribe";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}