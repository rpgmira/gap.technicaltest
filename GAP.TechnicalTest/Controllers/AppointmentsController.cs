﻿using GAP.ApplicationCore.Entities;
using GAP.ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace GAP.TechnicalTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppointmentsController : ControllerBase
    {
        IAppointmentService _appointmentService;

        public AppointmentsController(IAppointmentService appointmentService)
        {
            _appointmentService = appointmentService;
        }

        // GET: api/Appointments
        [HttpGet]
        public IEnumerable<Appointment> Get()
        {
            var result = _appointmentService.GetAll();

            return result;
        }

        // POST: api/Appointments
        [HttpPost]
        public bool Post([FromBody] Appointment value)
        {
            var result = _appointmentService.Create(value);

            return result;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            var result = _appointmentService.Remove(id);

            return result;
        }
    }
}