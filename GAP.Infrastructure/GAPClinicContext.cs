﻿using GAP.ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;

namespace GAP.Infrastructure
{
    public class GAPClinicContext : DbContext
    {
        // cd GAP.Infrastructure
        // dotnet ef migrations add InitialCreate
        // dotnet ef database update

        private const string _mySqlConnectionString = "server=dracohobby.com;database=draco_GAPClinic;user=draco_rpgmira;password=Magic123$";

        private const string _sqliteConnectionString = "Data Source=GAPClinic.db";

        public DbSet<Patient> Patient { get; set; }
        public DbSet<Appointment> Appointment { get; set; }
        public DbSet<Specialty> Specialty { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseMySql(_mySqlConnectionString);
            optionsBuilder.UseSqlite(_sqliteConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Specialty Data Seeding

            modelBuilder.Entity<Specialty>().HasData(new Specialty { Id = 1, Name = "General Medicine" });
            modelBuilder.Entity<Specialty>().HasData(new Specialty { Id = 2, Name = "Dentistry" });
            modelBuilder.Entity<Specialty>().HasData(new Specialty { Id = 3, Name = "Pediatrics" });
            modelBuilder.Entity<Specialty>().HasData(new Specialty { Id = 4, Name = "Neurology" });

            #endregion
        }
    }
}