﻿using GAP.ApplicationCore.Entities;
using GAP.ApplicationCore.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace GAP.Infrastructure.Repositories
{
    public class Repository<T> : IRepository<T> where T : EntityBase
    {
        private readonly GAPClinicContext _dbContext;

        public Repository()
        {
            _dbContext = new GAPClinicContext();
        }

        public bool Add(T entity)
        {
            _dbContext.Set<T>().Add(entity);
            var changes = _dbContext.SaveChanges();

            return changes > 0;
        }

        public bool Delete(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
            var changes = _dbContext.SaveChanges();

            return changes > 0;
        }

        public bool Edit(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            var changes = _dbContext.SaveChanges();

            return changes > 0;
        }

        public T GetById(int id)
        {
            return _dbContext.Set<T>().Find(id);
        }

        public IEnumerable<T> List()
        {
            return _dbContext.Set<T>().AsEnumerable();
        }
    }
}